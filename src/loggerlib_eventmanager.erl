%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Event Manager by log-type for event handler.
%%%      Simply OTP structure element: gen_event
%%% -------------------------------------------------------------------

-module(loggerlib_eventmanager).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1,
         stop/1,
         add_sup_handler/3,
         delete_handler/3,
         notify/2,
         sync_notify/2,
         call/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Name) ->
    gen_event:start_link({local, Name}).

stop(Name) ->
    gen_event:stop(Name).

%% -----------------------

add_sup_handler(Name, Handler, InitArgs) ->
    gen_event:add_sup_handler(Name, Handler, InitArgs).

delete_handler(Name, Handler, TerminateArgs) ->
    gen_event:delete_handler(Name, Handler, TerminateArgs).

notify(Name, Event) ->
    gen_event:notify(Name, Event).

sync_notify(Name, Event) ->
    gen_event:notify(Name, Event).

call(Name, Handler, Request) ->
    gen_event:call(Name, Handler, Request).

%% ====================================================================
%% Internal functions
%% ====================================================================


