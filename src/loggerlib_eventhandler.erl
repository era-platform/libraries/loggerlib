%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Event Handler by log-type.
%%%      Simply OTP structure sub-element: gen_event's handler.
%%% -------------------------------------------------------------------

-module(loggerlib_eventhandler).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_event).

-export([init/1, handle_event/2, handle_call/2, handle_info/2, terminate/2, code_change/3, format_status/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Types
%% ====================================================================

-record(state, {
    hid, % gen_event regname
    dir, % directory for file
    fileprefix, % generic file name
    sep_pids, % separate pids by writing line
    cur_filedtname, % dated file name
    cur_filepart, % index of dated file
    cur_filepath, % current open file path
    cur_timestart, % time start of current file
    cur_size, % current file size
    iodevice, % open iodevice
    last_w_pid, % last writed pid
    filter % count of messages to skip (due overload)
  }).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% -----------------------------
%% Init
%% -----------------------------
init(Opts) ->
    [HandlerId,Dir,Prefix] = ?BU:extract_required_props([hid,dir,file], Opts),
    State = #state{hid=HandlerId,
                   dir=Dir,
                   fileprefix=Prefix,
                   sep_pids=false,
                   cur_filepart=-1,
                   cur_filedtname=undefined,
                   cur_filepath=undefined,
                   cur_timestart=undefined,
                   iodevice=undefined,
                   last_w_pid=undefined,
                   filter=#{skipcount => 0,
                            lastsec => {0,0,0},
                            lastseccount => 0,
                            lastsecfiltered => 0}},
    ?LOG('$trace', "Log ~p init: ~120p", [HandlerId, Opts]),
    Now=?BU:localdatetime_ms(),
    open_file(Now, State).

%% -----------------------------
%% Event
%% -----------------------------
handle_event({w,Time,Pid,Fun}=_Event, #state{}=State) when is_function(Fun) ->
    {ok, do_write(Time,Pid,Fun(),State)};
handle_event({w,Time,Pid,Txt}=_Event, #state{}=State) ->
    {ok, do_write(Time,Pid,Txt,State)};

handle_event({next_date,Time}=_Event, #state{}=State) ->
    {ok, next_date(Time,State)};

handle_event(_Event, #state{hid=Hid}=State) ->
    ?LOG('$info', "Log ~p unknown event: ~p", [Hid, _Event]),
    {ok, State}.

%% -----------------------------
%% Call
%% -----------------------------
handle_call(_Msg, #state{hid=_Hid}=State) ->
    {noreply, State}.

%% -----------------------------
%% Info
%% -----------------------------
handle_info(_Msg, #state{hid=_Hid}=State) ->
    {noreply, State}.

%% -----------------------------
%% Terminate
%% -----------------------------
terminate(_Arg, #state{fileprefix=_Prefix, hid=Hid, iodevice=undefined}=_State) ->
    ?LOG('$trace', "Log ~p terminate: ~120p", [Hid, _Arg]),
    ok;
terminate(_Arg, #state{fileprefix=_Prefix, hid=Hid}=State) ->
    ?LOG('$trace', "Log ~p terminate: ~120p", [Hid, _Arg]),
    close_file(State), % @todo sending log to store every time on exception, something else should be done
    ok.

%% -----------------------------
%% Code change
%% -----------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% -----------------------------
%% Format status
%% -----------------------------
format_status(normal, [_PDict, #state{}=State]) -> State;
format_status(terminate, [_PDict, #state{}=State]) -> State.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------
%% Open file
%% ------------------------

%% create dir
%% search max file in dir
%% - create first, goto x
%% - take last
%%   - check size
%%     - overhead -> create next, goto x
%%     - insize -> take, goto x
%% x: try open
%%     try write
%%  return

open_file(Now, #state{dir=Dir}=State) ->
    ?LOG('$trace', "Log ~p. Open file...", [State#state.hid]),
    case filelib:ensure_dir(Dir) of
        {error, _Reason}=Error -> Error;
        ok -> open_file_1(Now, State)
    end.

open_file_1(Now, #state{dir=Dir,fileprefix=Prefix,cur_filedtname=CurFnDt,cur_filepart=CurPart}=State) ->
    FnDt = get_file_name(Prefix, Now),
    LogMaxFileCount = ?CFG:role_log_store_file_count(),
    filter_existing_files(Dir, Prefix, FnDt, LogMaxFileCount),
    case check_existing_files(Dir, FnDt) of
        {-1,undefined} ->
            FilePath = get_file_path(Dir,FnDt,0),
            open_file_2(FilePath, {Now,FnDt,0}, State);
        {N,FilePath} when N>=0 andalso (FnDt=/=CurFnDt orelse N>CurPart) ->
            open_file_2(FilePath, {Now,FnDt,N}, State);
        {N,_} when N>=0 ->
            FilePath = get_file_path(Dir,FnDt,CurPart+1),
            open_file_2(FilePath, {Now,FnDt,0}, State)
    end.

open_file_2(FilePath, {Now,FNDt,N}, #state{dir=Dir}=State) ->
    case filelib:file_size(FilePath) of
        _Size when _Size >= ?LOGMAXSIZE ->
            FilePath1 = get_file_path(Dir,FNDt,N+1),
            open_file_2(FilePath1, {Now,FNDt,N+1}, State);
        Size ->
            open_file_3(FilePath, {Now,FNDt,N,Size}, State)
    end.

open_file_3(FilePath, {Now,FNDt,N,Size}, State) ->
    %case file:open(FilePath, [append, {delayed_write,16384,1000}, {encoding, utf8}]) of
    case file:open(FilePath, [raw, append, {delayed_write,16384,1000}]) of
        {error, Reason}=Error ->
            ?LOG('$error', "Log ~p. File open error ~120p. Reason=~120p", [State#state.hid, FilePath, Reason]),
            Error;
        {ok, F} ->
            ?LOG('$trace', "Log ~p. File opened ~120p (size=~pKB)", [State#state.hid, FilePath, Size div 1024]),
            {{Y,M,D}, {H,Mi,S}=_Time, Ms}=Now,
            Node = node(),
            B1 = ?FORMAT("~n===================================================================================================~n",[]),
            B2 = ?FORMAT("======== Log started at ~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B.~3..0B ===================================================~n",[Y,M,D,H,Mi,S,Ms]),
            B3 = ?FORMAT("======== Node: ~ts~n",[Node]),
            B4 = ?FORMAT("===================================================================================================~n~n",[]),
            file:write(F,<<B1/bitstring,B2/bitstring,B3/bitstring,B4/bitstring>>),
            {ok, State#state{cur_timestart=Now,
                             cur_filepart=N,
                             cur_filepath=FilePath,
                             cur_size=Size+300+1+300+2+size(?BU:to_binary(Node))+17,
                             cur_filedtname=FNDt,
                             iodevice=F}}
    end.

check_existing_files(Dir, FnDt) ->
    F = fun(FilePath, {AccN,_AccFN}=Acc) ->
            BaseName = filename:basename(FilePath),
            {_,F2}=lists:split(length(FnDt), BaseName),
            case list_to_integer(lists:takewhile(fun($.)->false;(_)->true end, lists:reverse(lists:droplast(lists:reverse(F2))))) of
                _N when _N<AccN -> Acc;
                N when N>AccN -> {N, FilePath}
            end end,
    Reg = "^" ++ FnDt ++ "\\.\\d+\\.log",
    filelib:fold_files(Dir, Reg, false, F, {-1,undefined}).

filter_existing_files(Dir, Prefix, FnDt, LeaveCount) ->
    F = fun(FilePath, Acc) ->
            BaseName = filename:basename(FilePath),
            {FnDt2,F}=lists:split(length(FnDt), BaseName),
            N = list_to_integer(lists:takewhile(fun($.)->false;(_)->true end, lists:reverse(lists:droplast(lists:reverse(F))))),
            %[{N,FilePath}|Acc]
            [{FnDt2,N,FilePath}|Acc]
        end,
    %Reg = "^" ++ FnDt ++ "\\.\\d+\\.log", % only current date date
    Reg = "^" ++ Prefix ++ "_\\d{4}-\\d{2}-\\d{2}\\.\\d+\\.log", % all dates
    AllFiles = lists:sort(filelib:fold_files(Dir, Reg, false, F, [])),
    DelCount = case length(AllFiles)-LeaveCount of
                   _N when _N<0 -> 0;
                   N -> N
               end,
    {ToDel,_} = lists:split(DelCount, AllFiles),
    lists:foreach(fun({_,_,FilePath}) ->
                          ?LOG('$trace', "Deleting expired file ~120p", [FilePath]),
                          file:delete(FilePath)
                  end, ToDel).

get_file_name(Prefix, {{Y,M,D},_Time,_Ms}=_Now) ->
    ?BU:str("~s_~4..0B-~2..0B-~2..0B", [Prefix,Y,M,D]).

get_file_path(Dir, FnDt, N) ->
    Fmt = case N of
               _ when N<10000 -> "~4..0B";
              _ -> "~B"
          end,
    Dir ++ FnDt ++ "." ++ ?BU:str(Fmt, [N]) ++ ".log".

%% ------------------------
%% Close file
%% ------------------------
close_file(#state{iodevice=F}=State) ->
    ?LOG('$trace', "Log ~p. File closed: ~p", [State#state.hid, State#state.cur_filepath]),
    {{Y,M,D}, {H,Mi,S}=_Time, Ms}=?BU:localdatetime_ms(),
    B1 = ?FORMAT("~n===================================================================================================~n",[]),
    B2 = ?FORMAT("======== Log closed at ~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B.~3..0B ====================================================~n",[Y,M,D,H,Mi,S,Ms]),
    B3 = ?FORMAT("===================================================================================================~n~n",[]),
    file:write(F,<<B1/bitstring,B2/bitstring,B3/bitstring>>),
    file:close(F),
    ?LOGSRV:log_closed(State#state.cur_filepath),
    State#state{iodevice=undefined,
                cur_size=0,
                last_w_pid=undefined,
                cur_filepath=undefined}.

%% ------------------------
%% Write
%% ------------------------
do_write(_Time, _Pid, _Txt, #state{filter=#{skipcount:=SkipCount}=Filter}=State) when SkipCount > 0 ->
    State#state{filter=Filter#{skipcount:=SkipCount-1}};
do_write(Time, Pid, Txt,  #state{filter=Filter}=State) ->
    case process_info(self(), message_queue_len) of
        {_,Q} when Q < ?QUEUELIMIT ->
            do_write_1(Time, Pid, Txt, State);
        _ ->
            State1 = write_1(Time, self(),  "------------------ " ++ ?BU:to_list(?QUEUESKIP) ++ " lines skipped by queue overload ! -----------------", State),
            State1#state{filter=Filter#{skipcount:=?QUEUESKIP}}
    end.

do_write_1({H,M,S,_}=_Time, _Pid, _Txt, #state{filter=#{lastsec:={H,M,S}, lastseccount:=LCount}=Filter}=State) when LCount > ?SECLIMIT ->
    #{lastsecfiltered:=LFiltered}=Filter,
    State#state{filter=Filter#{lastsecfiltered:=LFiltered+1}};
do_write_1({H,M,S,_}=Time, Pid, Txt, #state{filter=#{lastsec:={H,M,S}}=Filter}=State)  ->
    #{lastseccount:=LCount}=Filter,
    State1 = write(Time, Pid, Txt, State),
    State1#state{filter=Filter#{lastseccount:=LCount+1}};
do_write_1({H,M,S,_}=Time, Pid, Txt, #state{filter=#{lastsecfiltered:=LFiltered}=Filter}=State) when LFiltered == 0 ->
    State1 = write(Time, Pid, Txt, State),
    State1#state{filter=Filter#{lastsec:={H,M,S}, lastseccount:=1, lastsecfiltered:=0}};
do_write_1({H,M,S,_}=Time, Pid, Txt, #state{filter=Filter}=State) ->
    #{lastsec:={LH,LM,LS}, lastsecfiltered:=LFiltered}=Filter,
    State1 = write_1({LH,LM,LS,999}, self(),  "------------------ " ++ ?BU:to_list(LFiltered) ++ " lines skipped by second limit ! -----------------", State),
    State2 = write(Time, Pid, Txt, State1),
    State2#state{filter=Filter#{lastsec:={H,M,S}, lastseccount:=1, lastsecfiltered:=0}}.

%% -----
write(Time, Pid, Txt, State)
  when is_list(Txt) ->
    write_1(Time, Pid, Txt, State);
write(Time, Pid, {Fmt,Args}, State)
  when is_list(Fmt), is_list(Args) ->
    Txt = try ?BU:str(Fmt, Args)
          catch _:_ -> ?BU:str("Error log format/args (~s, ~120tp)", [Fmt,Args])
          end,
    write_1(Time, Pid, Txt, State).

%% conditional writing
write_1(Time, Pid, Txt, #state{cur_size=Size}=State) when Size>=?LOGMAXSIZE ->
    State1 = close_file(State),
    Now = ?BU:localdatetime_ms(),
    {ok, State2} = open_file(Now, State1),
    write_1(Time, Pid, Txt, State2);

write_1(Time, Pid, Txt, #state{last_w_pid=LPid, sep_pids=Sep, cur_size=Size}=State) when LPid==Pid; LPid==undefined; Sep==false->
    Size1 = write_to_file(Time, Pid, Txt, State),
    State#state{cur_size=Size+Size1,
                last_w_pid=Pid};

write_1(Time, Pid, Txt, #state{iodevice=F, cur_size=Size, sep_pids=true}=State) ->
    B1 = ?BU:strbin("-----------------------------~n",[]),
    file:write(F, B1),
    Size1 = write_to_file(Time, Pid, Txt, State),
    State#state{cur_size=Size+Size1+byte_size(B1),
                last_w_pid=Pid}.

%% direct writing
write_to_file(Time, Pid, Txt, #state{iodevice=F}) ->
    {H,M,S,Ms}=Time,% ?BU:localtime_ms(),
    B1 = ?BU:strbin("~2..0B:~2..0B:~2..0B.~3..0B\t~p \t",[H,M,S,Ms,Pid]),
    B2 = ?BU:to_binary(Txt),
    case file:write(F,<<B1/bitstring,B2/bitstring,"\n">>) of
        ok -> byte_size(B1) + byte_size(B2) + 1;
        {error, _} -> 0
    end.

%% --------------------------
%% Next Date
%% --------------------------
next_date(Now, State) ->
    State1 = close_file(State),
    {ok, State2} = open_file(Now, State1),
    State2.
