%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.07.2020
%%% @doc get configuration parameters
%%% -------------------------------------------------------------------

-module(loggerlib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([log_destination/1]).

-export([min_free_disk_space/0,
         role_log_store_days/0,
         role_log_store_max_size/0,
         role_log_store_file_count/0,
         move_to_filestorage_function/0]).

-export([get_log_dir/1,
         basic_log_dir/0]).

-export([get_env/2,
         get_all_env/0,
         set_env/2,
         unset_env/1,
         update_env/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% log_destination
log_destination(Default) ->
    get_env('log_destination', Default).

%% -----------------------------------------
-spec min_free_disk_space() -> DiskSpace::integer().
%% -----------------------------------------
min_free_disk_space() ->
    DiskSpace = get_env('min_free_disk_space',?LOGMINDISKSPACE),
    ?BU:to_int(DiskSpace*1024*1024). % to KB

%% -----------------------------------------
-spec role_log_store_days() -> Days::integer().
%% -----------------------------------------
role_log_store_days() ->
    get_env('role_log_store_days',?LOGSTOREDAYS).

%% -----------------------------------------
-spec role_log_store_max_size() -> MaxSizeBytes::integer().
%% -----------------------------------------
role_log_store_max_size() ->
    case get_env('role_log_store_max_size',undef) of
        undef -> ?LOGMAXSIZE*?LOGMAXFILECOUNT; % in B
        Val -> ?BU:to_int(Val*1024*1024*1024) % to B
    end.

%% -----------------------------------------
-spec role_log_store_file_count() -> FileCount::integer().
%% -----------------------------------------
role_log_store_file_count() ->
    case get_env('role_log_store_file_count',undef) of
        undef -> ?LOGMAXFILECOUNT; % default
        Val -> Val
    end.

%% -----------------------------------------
-spec move_to_filestorage_function() -> function().
%% -----------------------------------------
move_to_filestorage_function() ->
    case get_env('move_to_filestorage_function',undef) of
        Val when is_function(Val,1) -> Val;
        _ -> fun(_) -> ok end
    end.

%% -----------------------------------------
%% basic_log_dir
%% -----------------------------------------
basic_log_dir() ->
    get_env('basic_log_dir',"log").


%% -----------------------------------------
%%
%% -----------------------------------------
get_log_dir({X,X}) -> get_log_dir(X);
get_log_dir({Dir,_Pref}) -> basic_log_dir() ++ "/" ++ ?BU:to_list(Dir) ++ "/";
get_log_dir({Dir1,Dir2,_Pref}) ->  basic_log_dir() ++ "/" ++ ?BU:to_list(Dir1) ++ "/" ++ ?BU:to_list(Dir2) ++ "/";
get_log_dir(Code) -> basic_log_dir() ++ "/" ++ ?BU:to_list(Code) ++ "/".

%% ====================================================================
%% Internal functions
%% ====================================================================

get_env(Key,{fn,_}=Default) -> ?BU:get_env(?APP,Key,Default);
get_env(Key,Default) -> ?BU:get_env(?APP,Key,Default).
get_all_env() -> ?BU:get_all_env(?APP).
set_env(Key,Value) -> ?BU:set_env(?APP,Key,Value).
unset_env(Key) -> ?BU:unset_env(?APP,Key).
update_env(Opts) -> ?BU:update_env(?APP,Opts).
