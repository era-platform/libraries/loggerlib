%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Gen_server of logger, responsible for start/stop handlers, removing files, new_date timer.
%%% -------------------------------------------------------------------

-module(loggerlib_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, stop/0, restart/0]).
-export([write/2]).
-export([log_closed/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Types
%% ====================================================================

-record(state, {
    logETS,
    currentDt,
    sentETS,
    paused
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

stop() ->
    gen_server:stop(?MODULE).

restart() ->
    gen_server:cast(?MODULE, {restart}).

%% ---------------------------------
%% Write facade
%% ---------------------------------
write(Code, Text)
  when is_atom(Code) orelse is_binary(Code) orelse is_list(Code) ->
    write_1(Code,Text);
write({Dir,Pref}=Code, Text)
  when (is_atom(Dir) orelse is_binary(Dir) orelse is_list(Dir))
  andalso (is_atom(Pref) orelse is_binary(Pref) orelse is_list(Pref)) ->
    write_1(Code,Text);
write({Dir1,Dir2,Pref}=Code, Text)
  when (is_atom(Dir1) orelse is_binary(Dir1) orelse is_list(Dir1))
  andalso (is_atom(Dir2) orelse is_binary(Dir2) orelse is_list(Dir2))
  andalso (is_atom(Pref) orelse is_binary(Pref) orelse is_list(Pref)) ->
    write_1(Code,Text).

%% @private
write_1(Code, Text) ->
    gen_server:cast(?MODULE, {w,Code,{?BU:localtime_ms(),self(),Text}}).

%% ---------------------------------
%% handle log file closed event
%% ---------------------------------
log_closed(FilePath) ->
    gen_server:cast(?MODULE, {log_closed,FilePath}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Now = os:timestamp(),
    [SETS] = ?BU:extract_required_props([sets], Opts),
    LETS = ets:new(logs,[set]), % need to restart loghandlers when fall down
    State = #state{logETS=LETS,
                   currentDt=?BU:localdate(Now),
                   sentETS=SETS,
                   paused=false},
    %
    Self = self(),
    erlang:send_after(0, Self, {timer_check_diskspace}),
    erlang:send_after(0, Self, {timer_check_newdate}),
    erlang:send_after(30000, Self, {filter_files}),
    erlang:send_after(5000, Self, {timer_copy_logs}),
    %
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({print_state}, _From, State) ->
    ?OUTC('$force',"LogSrv. print: ~140p", [State]),
    {reply, ok, State};

handle_call({close_all}, _From, #state{logETS=ETS}=State) ->
    ?OUTC('$force',"LogSrv. close_all"),
    ets:foldl(fun({_Code,EvManName}, Acc) ->
                      ?LOGEVSUPV:terminate_child(EvManName),
                      ?LOGEVSUPV:delete_child(EvManName),
                      Acc+1
              end, 0, ETS),
    ets:delete_all_objects(ETS),
    {reply, ok, State};

handle_call({restart}, _From, State) ->
    ?OUTC('$force',"LogSrv. restart"),
    {stop, restart, ok, State};

handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({w,_,_}, #state{paused=true}=State) -> {noreply, State};

handle_cast({w,Code,{Time,Pid,Text}}, #state{}=State) ->
    case get_log(Code,State) of
        {error,_Reason}=Err ->
            ?LOG('$error', "error: ~120p", [_Reason]),
            Err;
        Name ->
            ?LOGEVMAN:notify(Name,{w,Time,Pid,Text})
    end,
    {noreply, State};

handle_cast({log_closed,FilePath}, #state{sentETS=SETS}=State) ->
    ets:insert(SETS, {FilePath,?BU:current_gregsecond()}),
    spawn(fun() -> do_copy_log_to_store(FilePath) end),
    {noreply, State};

handle_cast({restart}, #state{}=State) ->
    {stop, restart, State};

handle_cast(_Msg, #state{}=State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({gen_event_EXIT, {_M,{file,Code}=_HId}=_H, Reason}, #state{logETS=ETS}=State) ->
    ?LOG('$warning', "LogSrv. loghandler exited: ~p, (~120p)", [Code, Reason]),
    ets:delete(ETS, Code),
    {noreply, State};

handle_info({timer_check_newdate}, State) ->
    State1 = check_newdate(?BU:localdatetime_ms(), State),
    {noreply, State1};

handle_info({timer_copy_logs}, State) ->
    Self = self(),
    spawn_link(fun() -> try do_copy_unknown_logs(State),
                            do_filter_sent_ets(State),
                            do_delete_unknown_logs_bysize()
                        catch A:B -> ?OUT('$error',"LogSrv. timer_copy_logs error ~120tp:~120tp", [A,B])
                        end,
                        erlang:send_after(30000, Self, {timer_copy_logs})
                 end),
    {noreply, State};

handle_info({filter_files}, State) ->
    spawn(fun() -> do_filter_files() end),
    {noreply, State};

handle_info({timer_check_diskspace}, State) ->
    State1 = check_diskspace(State),
    Self = self(),
    erlang:send_after(60000, Self, {timer_check_diskspace}),
    {noreply, State1};

handle_info(_Msg, #state{}=State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% returns log event-manager's name
get_log(Code, #state{logETS=ETS}=State) ->
    case ets:lookup(ETS, Code) of
        [{_,EvManName}] -> EvManName;
        [] ->
            case open_log(Code, State) of
                {error,_}=Err -> Err;
                Res -> Res
            end
    end.

%% creates log
open_log(Code, #state{logETS=ETS}=_State) ->
    ?LOG('$trace', "LogSrv open_log ~120p", [Code]),
    EvManName = get_log_event_manager_name(Code),
    ChildSpec = {EvManName, {?LOGEVMAN, start_link, [EvManName]}, permanent, 1000, worker, [?LOGEVMAN]},
    ?LOGEVSUPV:start_child(ChildSpec),
    case add_file_handler(Code, EvManName) of
        {error,_}=Err -> Err;
        ok ->
            ets:insert(ETS, {Code, EvManName}),
            % to avoid double log writing if x and {x,x}
            ets:insert(ETS, {EvManName, EvManName}),
            EvManName
    end.

%% adds file handler to log
add_file_handler(Code, EvManName) ->
    HandlerId = {file,Code},
    Handler = {?LOGEVHAN,HandlerId},
    InitArgs = [{hid,HandlerId},
                {dir,?CFG:get_log_dir(Code)},
                {file,get_filename_prefix(Code)}],
    ?LOGEVMAN:add_sup_handler(EvManName, Handler, InitArgs).

%
get_log_event_manager_name({X,X}) -> get_log_event_manager_name(X);
get_log_event_manager_name({Dir,Pref}) -> ?BU:to_atom_new(?BU:to_list(Dir)++"|"++?BU:to_list(Pref)++">");
get_log_event_manager_name({Dir1,Dir2,Pref}) -> ?BU:to_atom_new(?BU:to_list(Dir1)++"|"++?BU:to_list(Dir2)++"|"++?BU:to_list(Pref)++">");
get_log_event_manager_name(Code) -> ?BU:to_atom_new(?BU:to_list(Code)++">").

%
get_filename_prefix({X,X}) -> get_filename_prefix(X);
get_filename_prefix({_Dir,Pref}) -> ?BU:to_list(Pref);
get_filename_prefix({_Dir1,_Dir2,Pref}) -> ?BU:to_list(Pref);
get_filename_prefix(Code) -> ?BU:to_list(Code).

%% ---------------------------------
%% Checking and updating filenames due to next date
%% ---------------------------------

%% checks time, difference of date, starts timer
check_newdate({Date, {H,Mi,S}, _Ms}, #state{currentDt=Date}=State) when H<23; Mi<59; S<54 ->
    erlang:send_after(5000, self(), {timer_check_newdate}),
    State;
check_newdate({Date, {_H,_Mi,S}, Ms}, #state{currentDt=Date}=State) ->
    erlang:send_after(60000-S*1000-Ms+10, self(), {timer_check_newdate}),
    State;
check_newdate({Date1, _Time, _Ms}=Now, State) ->
    update_files(Now, State),
    %
    erlang:send_after(30000, self(), {filter_files}),
    erlang:send_after(5000, self(), {timer_check_newdate}),
    %
    State#state{currentDt=Date1}.

%% ---------------------------------
%% sends next_date command to all log handlers
%% ---------------------------------
update_files(Now, State) ->
    ?LOG('$trace', "New date detected"),
    LogF = fun({_Code,EvManName}) -> ?LOG('$trace', "New date to ~100p", [EvManName]) end,
    notify_all({next_date,Now}, LogF, State).

%% @private
notify_all(Msg, LogF, #state{logETS=ETS}) ->
    F = fun({Code,EvManName}, {Acc,DAcc}) ->
                case maps:find(EvManName, DAcc) of
                    error ->
                        case is_function(LogF,1) of
                            true -> LogF({Code,EvManName});
                            false -> ok
                        end,
                        ?LOGEVMAN:notify(EvManName, Msg),
                        {Acc+1,maps:put(EvManName,0,DAcc)};
                    _ -> {Acc,DAcc}
                end end,
    {Cnt,_} = ets:foldl(F, {0,maps:new()}, ETS),
    Cnt.

%% ----------------------------------
%% check disk space usage
%% ---------------------------------
check_diskspace(#state{paused=Paused}=State) ->
    {ok,CWD} = file:get_cwd(),
    BasicLogDir = filename:join(CWD,?CFG:basic_log_dir()),
    case ?BU:get_disk_space(BasicLogDir) of
        undefined -> State;
        {_,Cap,Perc}=R ->
            case Cap - (Cap div 100 * Perc) > ?CFG:min_free_disk_space() of % defaulr=3 GB
                true when Paused -> unpause_logs(R,State);
                false when not Paused -> pause_logs(R,State);
                _ -> State
            end end.

%% @private
pause_logs(R,State) ->
    {ok,CWD} = file:get_cwd(),
    BasicLogDir = filename:join(CWD,?CFG:basic_log_dir()),
    ?OUT('$info',"LOG. Pause logs (total/used space: ~120p)",[R]),
    ?OUT('$info',"LOG. Disk data: ~120p", [disksup:get_disk_data()]),
    ?OUT('$info',"LOG. BasicLogDir: ~120p", [BasicLogDir]),
    ?OUT('$info',"LOG. CurrentDir: ~120p", [CWD]),
    Msg = {w,?BU:localtime_ms(),self(),"Log paused (not enough free space)"},
    notify_all(Msg, undefined, State),
    State#state{paused=true}.

%% @private
unpause_logs(R,State) ->
    ?OUT('$info',"LOG. Unpause logs (free space: ~120p)",[R]),
    Msg = {w,?BU:localtime_ms(),self(),"Log unpaused"},
    notify_all(Msg, undefined, State),
    State#state{paused=false}.

%% ---------------------------------
%% filter old files (every new date)
%% ---------------------------------
do_filter_files() ->
    BasicDir = ?CFG:basic_log_dir(),
    StoreDays = ?CFG:role_log_store_days(),
    Now = calendar:now_to_local_time(os:timestamp()),
    ?LOG('$trace', "Filter old files ~140p, store_days=~p",[BasicDir, StoreDays]),
    Fsearch = fun(FilePath, Acc) ->
                    case filelib:last_modified(FilePath) of
                        0 -> ok;
                        FD ->
                            case calendar:time_difference(FD, Now) of
                                {Days,_Time} when Days >= StoreDays -> [FilePath|Acc];
                                _ -> Acc
                            end end end,
    ToDel = filelib:fold_files(BasicDir, ".*", true, Fsearch, []),
    Fdel = fun(FilePath) ->
                   case file:delete(FilePath) of
                       ok -> ?LOG('$trace', "Old file deleted: ~140p",[FilePath]);
                       {error,_Reason} -> ?LOG('$error', "file delete error: ~140p -> ~140p",[FilePath,_Reason])
                   end end,
    lists:foreach(Fdel, ToDel),
    timer:sleep(1000),
    ?BU:directory_delete_empties(BasicDir).

%% ---------------------------------
%% spawned send log file name on log store
%% ---------------------------------
do_copy_log_to_store(FilePath) ->
    F = ?CFG:move_to_filestorage_function(),
    F(FilePath).

%% ---------------------------------
%% spawned check of closed log files (by name, followed by next file of this type in folder)
%% ---------------------------------
do_copy_unknown_logs(#state{sentETS=SETS}) ->
    BasicDir = ?CFG:basic_log_dir(),
    Now = calendar:now_to_local_time(os:timestamp()),
    NowGS = ?BU:current_gregsecond(),
    Fsearch = fun(Path, Acc) ->
                    {D,F} = {filename:dirname(Path), filename:basename(Path)},
                    case lists:takewhile(fun($_) -> false; (_) -> true end, F) of
                        F -> Acc;
                        Pref ->
                            X = filename:join(D, Pref),
                            case lists:keyfind(X,1,Acc) of
                                false -> [{X,F}|Acc];
                                {_,FL} when F > FL -> lists:keyreplace(X,1,Acc,{X,F});
                                _ -> Acc
                            end end end,
    Logs = filelib:fold_files(BasicDir, ".log", true, Fsearch, []),
    F2 = fun(Path, Acc) ->
                 case ets:lookup(SETS, Path) of
                     [_|_] -> Acc;
                     [] ->
                         {D,F} = {filename:dirname(Path), filename:basename(Path)},
                         case lists:takewhile(fun($_) -> false; (_) -> true end, F) of
                             F -> Acc;
                             Pref ->
                                 X = filename:join(D, Pref),
                                 case lists:keyfind(X,1,Logs) of
                                     false -> Acc;
                                     {_,F} -> Acc;
                                     {_,_} ->
                                         case filelib:last_modified(Path) of
                                             0 -> Acc;
                                             LWT ->
                                                 case calendar:time_difference(LWT, Now) of
                                                     {Days,_Time} when Days >= 1 -> Acc;
                                                     _ -> [Path|Acc]
                                                 end end end end end end,
    ToCopy = filelib:fold_files(BasicDir, ".log", true, F2, []),
    lists:foreach(fun(Path) ->
                          ets:insert(SETS, {Path,NowGS}),
                          do_copy_log_to_store(Path)
                  end, ToCopy).

%% ---------------------------------
%% spawned filter sent files from ets
%% ---------------------------------
do_filter_sent_ets(#state{sentETS=SETS}) ->
    NowGS = ?BU:current_gregsecond(),
    ets:foldl(fun({Path,GS},ok) when GS + 2*86400 < NowGS -> ets:delete(SETS, Path);
                 (_,_) -> ok
              end, ok, SETS).

%% ---------------------------------
%% spawned delete unknown log files (by size limit)
%% ---------------------------------
do_delete_unknown_logs_bysize() ->
    BasicDir = ?CFG:basic_log_dir(),
    Fsearch = fun(Path, Acc) ->
                    {D,F} = {filename:dirname(Path), filename:basename(Path)},
                    case lists:takewhile(fun($_) -> false; (_) -> true end, F) of
                        F -> Acc;
                        Pref ->
                            X = filename:join(D, Pref),
                            FSize = filelib:file_size(Path),
                            FLWT = filelib:last_modified(Path),
                            case lists:keyfind(X,1,Acc) of
                                false -> [{X,D,[{FLWT,F,FSize}]}|Acc];
                                {_,D,FL} -> lists:keyreplace(X,1,Acc,{X,D,[{FLWT,F,FSize}|FL]})
                            end end end,
    Logs = filelib:fold_files(BasicDir, ".log", true, Fsearch, []),
    Logs1 = lists:map(fun({X,D,L}) -> {X,D,lists:sort(L)} end, Logs),
    MaxSize = ?CFG:role_log_store_max_size(),
    lists:foreach(fun({_,D,FL}) ->
                          case lists:foldl(fun({_,_,FSize},Acc) -> FSize + Acc end, 0, FL) of
                              TSize when TSize < MaxSize -> ok;
                              TSize -> lists:foldl(fun(_,ok) -> ok;
                                                      (_,Acc) when Acc < MaxSize -> ok;
                                                      ({_,F,FSize},Acc) ->
                                                           Path = filename:join(D, F),
                                                           ?LOG('$trace', "Deleting expired file ~120p", [Path]),
                                                           case file:delete(Path) of
                                                               ok -> Acc - FSize;
                                                               {error,_} -> Acc
                                                           end
                                                   end, TSize, FL)
                          end end, Logs1).

