%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Logger app general supervisor
%%% -------------------------------------------------------------------

-module(loggerlib_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Args) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, Args).

%% ====================================================================
%% Callbacks
%% ====================================================================

init(_Args) ->
    Opts = [{sets, ets:new(sent,[set, public])}],
    ChildSpec = [{srv, {?LOGSRV, start_link, [Opts]}, permanent, 1000, worker, [?LOGSRV]},
                 {supv, {?LOGEVSUPV, start_link, [[]]}, permanent, 5000, supervisor, [?LOGEVSUPV]}],
    {ok, {{one_for_all, 10, 2}, ChildSpec}}.
