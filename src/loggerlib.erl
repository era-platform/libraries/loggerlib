%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Logger application.
%%%   Application setups basiclib dependency callback option to handle log calls.
%%%   Application could be setup by application:set_env, by prepare(Opts), by start(Opts), by update_opts(Opts):
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','log'}
%%%      basic_log_dir
%%%         Absolute or relative path to log directory of node, where subdirectories should be located.
%%%         Default "log" (means $CWD/log)
%%%      min_free_disk_space
%%%         Size of free space on disk where log directory is located, in GB.
%%%         Default 3 GB
%%%      role_log_store_days
%%%         Limit of days to store local log files before auto deletioon.
%%%         Default 2
%%%      role_log_store_max_size
%%%         Limit of local log-files summary size, in GB.
%%%         Default 1 GB.
%%%      move_to_filestorage_function
%%%             :: function(Filepath::string())
%%%          Called to move closed log file to storage.
%%%          Default: no action
%%%   Facade functions:
%%%      write/2,
%%%      get_basic_log_dir/0,
%%%      get_log_dir/1
%%%   TODO:
%%%   TODO: make parameters:
%%%   TODO:   max_file_size, max_queue_size, skip_size, sec_limit,
%%%   TODO:   file_name_template, header_template, footer_template, line_prefix_template
%%% -------------------------------------------------------------------

-module(loggerlib).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0,
         stop/0]).

-export([start/2, stop/1]).

%% Facade functions
-export([write/2,
         get_basic_log_dir/0,
         get_log_dir/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ===================================================================
%% Private
%% ===================================================================

%% ====================================================================
%% Public
%% ====================================================================

%% --------------------------------------
%% Starts application
%% --------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% --------------------------------------
%% Stops application
%% --------------------------------------
stop() -> application:stop(?APP).

%% ====================================================================
%% Facade functions
%% ====================================================================

%% --------------------------------------
%% Write to selected log-file. Create and open file automatically.
%% --------------------------------------
-spec write(Code::atom()|binary()|string() |
                    {Dir::atom()|binary()|string(),Pref::atom()|binary()|string()} |
                    {Dir1::atom()|binary()|string(),Dir2::atom()|binary()|string(),Pref::atom()|binary()|string()},
                    Text :: string() | {string(),[any()]}) -> ok.
%% --------------------------------------
write(Code,Text) -> ?LOGSRV:write(Code,Text).

%% --------------------------------------
%% Return basic log directory (absolute or cwd-relative)
%% --------------------------------------
-spec get_basic_log_dir() -> string().
%% --------------------------------------
get_basic_log_dir() -> ?CFG:basic_log_dir().

%% --------------------------------------
%% Return log directory (absolute or cwd-relative)
%% --------------------------------------
-spec get_log_dir(Code::atom()|binary()|string() |
                          {Dir::atom()|binary()|string(),Pref::atom()|binary()|string()} |
                          {Dir1::atom()|binary()|string(),Dir2::atom()|binary()|string(),Pref::atom()|binary()|string()})
                          -> string().
%% --------------------------------------
get_log_dir(Code) -> ?CFG:get_log_dir(Code).


%% ====================================================================
%% Application callback functions
%% ====================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() ->
    % basiclib
    ?BU:set_env(?BASICLIB, log_function, fun(FileKey,Arg) -> ?MODULE:write(FileKey,Arg) end),
    ?BU:set_env(?BASICLIB, basic_log_dir, ?CFG:basic_log_dir()),
    ?BU:set_env(?BASICLIB, log_dir_function, fun(X) -> ?CFG:get_log_dir(X) end).

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent).