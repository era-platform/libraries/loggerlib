
%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).

-define(APP, loggerlib).
-define(SUPV, loggerlib_supv).

-define(CFG, loggerlib_config).
-define(LOGEVSUPV, loggerlib_ev_supv).
-define(LOGEVMAN, loggerlib_eventmanager).
-define(LOGEVHAN, loggerlib_eventhandler).
-define(LOGSRV, loggerlib_srv).

-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).

%% ====================================================================
%% Define other
%% ====================================================================

-define(FORMAT(Fmt,Args), list_to_binary(lists:flatten(io_lib:format(Fmt,Args)))).

-define(LOGMAXSIZE, 10485760). % one file up to 10 MB
-define(LOGMAXFILECOUNT, 100). % total file count up to 200 (for current and previous dates)
-define(LOGSTOREDAYS, 2). % delete file after lwt expired 2 days
-define(LOGMINDISKSPACE, 3). % min disk space for logging (in GB) - default.

-define(QUEUELIMIT, 5000). % if log queue contains more than 5000 messages - no write, but delete ...
-define(QUEUESKIP, 4500). % ... delete 4500 messages from queue
-define(SECLIMIT, 3000). % if current second was overloaded by 3000 messages - that's enough till next second

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).